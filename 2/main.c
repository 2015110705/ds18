/*
 * 2015110705 김민규
 * 본인은 이 소스파일을 다른 사람의 소스를 복사하지 않고 직접 작성하였습니다.
 */
#define _CRT_SECURE_NO_WARNINGS

#include <stdio.h>
#include <stdlib.h>

#define FALSE			0
#define TRUE			1

#define MAX_VERTICES	100

typedef struct qNode *queuePointer;
typedef struct qNode {
	int vertex;
	queuePointer link;
} tQNode;

typedef struct node* nodePointer;
typedef struct node
{
	int			vertex;
	nodePointer link;
}Node;

short int		visited[MAX_VERTICES];
nodePointer*	graph;

queuePointer	front, rear;

void			addq(int vertex);
int				deleteq();
int				queueEmpty();

void			bfs(int v);

nodePointer		makeNewNode(int data);
void			addToGraph(int index, nodePointer newNode);

int main()
{
	FILE*		inputFile = fopen("input.txt", "r");
	int			vertex, edge;
	nodePointer temp;
	int			inputData1, inputData2;
	int			i, j;

	/* input number of vertex and edge */
	fscanf(inputFile, "%d %d ", &vertex, &edge);

	/* graph memory allocation and initialization */
	graph = (nodePointer*)malloc(sizeof(nodePointer) * vertex);
	for (i = 0; i < vertex; i++) {
		graph[i] = NULL;
	}

	/* input edge and make list */
	for (i = 0; i < edge; i++) {
		fscanf(inputFile, "%d %d ",&inputData1, &inputData2);
		addToGraph(inputData1, makeNewNode(inputData2));
		addToGraph(inputData2, makeNewNode(inputData1));
	}
	fclose(inputFile);

	/* print adjacency list */
	printf("<<<<<<<<<< Adjacency List >>>>>>>>>> \n");
	for (i = 0; i < vertex; i++) {
		printf("graph[%d] : ", i);
		for (temp = graph[i]; temp != NULL; temp = temp->link) {
			printf("%3d ", temp->vertex);
		}
		printf("\n");
	}

	/* print result of dfs */
	printf("\n<<<<<<<<<< Depth First Search >>>>>>>>>>> \n");
	for (i = 0; i < vertex; i++) {
		printf("dfs<%d> : ", i);
		bfs(i);
		for (j = 0; j < vertex; j++) {
			visited[j] = FALSE;
		}
		printf("\n");
	}

	return 0;
}

/**
 * add item to the rear of queue i
 * @param	: vertex of item's vertex
 */
void addq(int vertex)
{
	queuePointer temp;
	
	temp = (queuePointer)malloc(sizeof(tQNode));
	temp->vertex = vertex;
	temp->link = NULL;

	if (front) {
		rear->link = temp;
	}
	else {
		front = temp;
	}
	rear = temp;
}

/**
 * delete an element from queue i
 */
int deleteq()
{
	queuePointer temp = front;
	int item;

	if (!temp) {
		return queueEmpty();
	}
	
	item = temp->vertex;
	front = temp->link;
	free(temp);

	return item;
}

int queueEmpty()
{
	fprintf(stderr, "queue is empty ");
	exit(EXIT_FAILURE);

	return NULL;
}

/**
 * breadth first traversal of a graph, starting at v
 * the global array visited is initialized to 0, the queue
 * operations are similar to those described in
 * Chapter 4, front and rear are global
 * @param	: index v
 */
void bfs(int v)
{
	nodePointer w;
	front = rear = NULL;	/* initialize queue */
	printf("%5d ", v);
	visited[v] = TRUE;
	addq(v);
	while (front) {
		v = deleteq();
		for (w = graph[v]; w; w = w->link) {
			if (!visited[w->vertex]) {
				printf("%5d ", w->vertex);
				addq(w->vertex);
				visited[w->vertex] = TRUE;
			}
		}
	}
}

/**
 * make new node function
 * @param	: data of new node
 */
nodePointer makeNewNode(int data)
{
	nodePointer newNode = (nodePointer)malloc(sizeof(Node));

	newNode->link = NULL;
	newNode->vertex = data;

	return newNode;
}

/**
 * add new node to graph array
 * @param	: graph array
 * @param	: array index
 * @param	: new node for addition 
 */
void addToGraph(int index, nodePointer newNode)
{
	newNode->link = graph[index];
	graph[index] = newNode;
}