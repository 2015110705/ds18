/*
 * 2015110705 김민규
 * 본인은 이 소스파일을 다른 사람의 소스를 복사하지 않고 직접 작성하였습니다.
 */
#define _CRT_SECURE_NO_WARNINGS

#include <stdio.h>
#include <stdlib.h>

#define FALSE			0
#define TRUE			1

#define MAX_VERTICES	100

typedef struct node* nodePointer;
typedef struct node
{
	int			vertex;
	nodePointer link;
}Node;

short int		visited[MAX_VERTICES];
nodePointer*	graph;

void			dfs(int v);
nodePointer		makeNewNode(int data);
void			addToGraph(int index, nodePointer newNode);
void connected(int vertex);

int main()
{
	FILE* inputFile = fopen("input.txt", "r");
	int vertex, edge;
	nodePointer temp;
	int inputData1, inputData2;
	int i, j;

	/* input number of vertex and edge */
	fscanf(inputFile, "%d %d ", &vertex, &edge);

	/* graph memory allocation and initialization */
	graph = (nodePointer*)malloc(sizeof(nodePointer) * vertex);
	for (i = 0; i < vertex; i++) {
		graph[i] = NULL;
	}

	/* input edge and make list */
	for (i = 0; i < edge; i++) {
		fscanf(inputFile, "%d %d ",&inputData1, &inputData2);
		addToGraph(inputData1, makeNewNode(inputData2));
		addToGraph(inputData2, makeNewNode(inputData1));
	}
	fclose(inputFile);

	/* print adjacency list */
	printf("<<<<<<<<<< Adjacency List >>>>>>>>>> \n");
	for (i = 0; i < vertex; i++) {
		printf("graph[%d] : ", i);
		for (temp = graph[i]; temp != NULL; temp = temp->link) {
			printf("%3d ", temp->vertex);
		}
		printf("\n");
	}

	/* print result of dfs */
	printf("\n<<<<<<<<<< Connected Components >>>>>>>>>>> \n");
	connected(vertex);

	return 0;
}

/**
 * depth first search of a graph beginning at v
 * @param	: beginning number v
 */
void dfs(int v)
{
	nodePointer w;
	visited[v] = TRUE;
	printf("%5d", v);
	for (w = graph[v]; w; w = w->link) {
		if (!visited[w->vertex]) {
			dfs(w->vertex);
		}
	}
}

/**
 * make new node function
 * @param	: data of new node
 */
nodePointer makeNewNode(int data)
{
	nodePointer newNode = (nodePointer)malloc(sizeof(Node));

	newNode->link = NULL;
	newNode->vertex = data;

	return newNode;
}

/**
 * add new node to graph array
 * @param	: graph array
 * @param	: array index
 * @param	: new node for addition 
 */
void addToGraph(int index, nodePointer newNode)
{
	newNode->link = graph[index];
	graph[index] = newNode;
}

/** 
 * determine the connected components of a graph 
 */
void connected(int vertex)
{
	int i;
	int count = 1;

	for (i = 0; i < vertex; i++) {
		if (!visited[i]) {
			printf("connected component %d : ", count);
			count++;
			dfs(i);
			printf("\n");
		}
	}
}